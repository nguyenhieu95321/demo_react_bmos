import React, {Component, useRef} from 'react';
import FRinput from "./FRinput";

const FrParent = () => {
    const inputRefs = useRef();
    return (
        <div>
            <FRinput ref={inputRefs}/>
            <button> Focus </button>
        </div>
    );

}

export default FrParent;