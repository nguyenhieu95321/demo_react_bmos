import React from 'react';
import {Button, Col, Form, Image, Input, Row, Typography} from 'antd';
import {useHistory} from 'react-router-dom'
import logo from "../../assets/images/logoblack.png"
import {ADMIN, ID, NAME} from "../value_const";

const {Title} = Typography;

export default function Login() {
    const history = useHistory()

    const logIn = () => {
        history.push(ADMIN)
    }
    return (
        <Row justify={"space-around"}>
            <Col span={8}>
                <div style={{
                    marginTop: 100,
                }}>
                    <Title style={{textAlign: 'center'}} level={3}>eFarm</Title>

                    <Form name="basic" labelCol={{span: 8,}} wrapperCol={{span: 16,}}>
                        <Form.Item
                            label="Username"
                            name="username"
                            rules={[{required: true, message: 'Bạn chưa nhập tên đăng nhập!',},]}
                        >
                            <Input/>
                        </Form.Item>

                        <Form.Item
                            label="Password"
                            name="password"
                            rules={[{required: true, message: 'Bạn chưa nhập tài khoản!',},]}
                        >
                            <Input.Password/>
                        </Form.Item>

                        <Form.Item wrapperCol={{offset: 8, span: 16,}}>
                            <Button type="primary" htmlType="submit" onClick={() => {
                                logIn()
                            }}>Đăng nhập</Button>
                        </Form.Item>
                    </Form>
                </div>
            </Col>
        </Row>
    );
}

