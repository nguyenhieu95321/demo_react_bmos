import React, {Component} from 'react';

const Counter = () => {
    const [count, setCount] = React.useState(0);
    const CounterPlus = () => {
        setCount(count + 1);
    }
    if (count === 5){
        throw new Error('Crashed !');
    }
    return (
        <div>
            <p>{count}</p>
            <button onClick={() => CounterPlus()}>count</button>
        </div>
    );
}

export default Counter;