import React, {Component} from 'react';

const HoverOpacity = ({children}) => {
    const [isHover, setIsHover] = React.useState(false);
    const onMountEnter = () => {
        setIsHover(true);
    }
    const onMountLeaver = () => {
        setIsHover(false)
    }
    return (
        <div
            style={{
                opacity: isHover ? 0.5 : 1
            }}

            onMouseEnter={() => onMountEnter()}
            onMouseLeave={() => onMountLeaver()}
        >
            {children}
        </div>
    );

}

export default HoverOpacity;