import React from 'react';
import {useHistory} from "react-router-dom";
import logo4 from "../../assets/images/logoblack.png"
import Device from "../Device";
import "../Device/style.css"
import {AppContext} from "../../Context/AppContext";
import Counter from "../Counter";
import ErrorBoundary from "../../ErrorBoundary";
import HOC from "../../HOC";
import SomeImages from "../someImages";
import HoverOpacity from "../hoverOpacity";
import Modal from "../modal";
import ReactPorto from "../ReactPorto";
import FrParent from "../FRParent";
import MessengerCustomerChat from 'react-messenger-customer-chat';




export default function Index() {
    // const [theme, setTheme] = React.useState('dark')
    const {theme, setTheme} = React.useContext(AppContext)
    const history = useHistory();
    const HOCSomeImages = HOC(SomeImages)
    const changeTheme = () => {
        setTheme(theme === 'dark' ? 'light' : 'dark');
    }
    return (
        <div className={"App"}>
            <FrParent/>
            <Modal/>
            <ReactPorto>Modal này ngoài div id root</ReactPorto>
            {/*<img src={logo4}/>*/}
            {/*<HoverOpacity>*/}
            {/*    <SomeImages src={logo4}/>*/}
            {/*</HoverOpacity>*/}
            {/*<HOCSomeImages src={logo4}/>*/}
            <button onClick={() => history.push('/')}>Đăng Xuất</button>
            <button className={theme} onClick={() => changeTheme()}>Chế độ tối</button>
            <ErrorBoundary>
                <Counter/>
            </ErrorBoundary>
            {/*<Device theme={theme}/>*/}
            <Device/>
            <MessengerCustomerChat
                pageId="103741495900484"
                appId="103741495900484"
            />
        </div>
    );
}

