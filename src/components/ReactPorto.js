import React, {Component} from 'react';
import ReactDOM from "react-dom";

const ReactPorto = ({children}) => {

    return ReactDOM.createPortal(
        <div>
            {children}
        </div>,
        document.querySelector("body")
    )
}

export default ReactPorto;